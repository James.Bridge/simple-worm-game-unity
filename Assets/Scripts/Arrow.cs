using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{

    public GameObject Apple;
    public GameObject Worm;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        // Store vector from worm to apple to ensure the correct rotation
        Vector3 vectorToApple = Apple.transform.position - Worm.transform.position;
        transform.LookAt(Apple.transform.position);

        // Only update arrow location if sufficiently far
        if (vectorToApple.magnitude > 2.5f)
        {

            // Ensure the arrow is always a distance of 2.5f away from the worm
            transform.position = Vector3.MoveTowards(Worm.transform.position, vectorToApple, 2.5f);

        }

    }
}
