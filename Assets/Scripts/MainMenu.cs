﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    // Function to load game scene from main menu
    public void PlayGame()
    {

        SceneManager.LoadScene("Game");

    }

    // Public function that allows for exiting the game
    public void Quit()
    {

        Application.Quit();

    }
}
