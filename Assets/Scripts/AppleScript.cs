﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AppleScript : MonoBehaviour
{

    public TextMeshProUGUI ScoreText;
    public GameObject Floor;
    public GameObject Worm;

    public int ScoreValue;
    private float FloorBound;

    void Start()
    {

        // Store the terrain size
        FloorBound = Floor.GetComponent<Terrain>().terrainData.size.x;
        RandomisePosition();

    }

    // Function to randomise the position of the apple
    public void RandomisePosition()
    {
        
        transform.position = new Vector3(
            Random.Range(0,FloorBound),
            Random.Range(-FloorBound / 8,FloorBound / 8),
            Random.Range(0,FloorBound)
            );
        // Debug.Log(transform.position);

    }

    void OnCollisionEnter(Collision collision)
    {

        // Handle player collision to increase score, relocate, and increase worm size
        if (collision.gameObject.tag == "Player")
        {
            ScoreValue++;
            ScoreText.text = "Score: " + ScoreValue.ToString();
            RandomisePosition();
            Worm.GetComponent<WormController>().GrowWorm();
        }

    }
    
    void LateUpdate()
    {

        // Just in case collisions cause unwanted movement we reset the velocities of the rigidbody.
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

    }

}
