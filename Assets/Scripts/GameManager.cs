﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{

    public GameObject Apple;
    public TextMeshProUGUI EndScoreText;
    public GameObject Menu;
    public GameObject PauseMenu;

    private int endScore;

    public void EndGame()
    {

        Menu.SetActive(true);
        endScore = Apple.GetComponent<AppleScript>().ScoreValue;
        DisplayScore();

    }

    private void DisplayScore()
    {

        EndScoreText.text = "End Score: " + endScore.ToString();

    }

    public void Restart()
    {

        Menu.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;

    }

    public void MainMenu()
    {

        SceneManager.LoadScene("Main Menu");

    }

    public void Pause()
    {

        Time.timeScale = 0;
        PauseMenu.SetActive(true);

    }

    public void Unpause()
    {

        Time.timeScale = 1;
        PauseMenu.SetActive(false);

    }

}