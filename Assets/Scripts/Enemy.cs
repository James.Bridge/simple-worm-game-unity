using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public GameObject Floor;
    public float MoveSpeed = 5f;
    public float RotateSpeed = 5f;
    public float SineSpeed = 3f;
    public float SineAmplitude = 50f;

    private float FloorBound;
    // Start is called before the first frame update
    void Start()
    {
        // Store terrain size to randomise location of Enemy.
        FloorBound = Floor.GetComponent<Terrain>().terrainData.size.x / 2;
        transform.position = new Vector3(
            Random.Range(0,FloorBound),
            Random.Range(-FloorBound / 8,FloorBound / 8),
            Random.Range(0,FloorBound)
            );
    }

    void OnCollisionEnter(Collision collision)
    {

        // Handle player collision causing game to end.
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Player Hit");
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            FindObjectOfType<GameManager>().EndGame();
        }

    }

    void Update()
    {

        // Cause constant movement in circles whilst oscillating vertically.
        transform.position += transform.forward * MoveSpeed * Time.deltaTime;
        transform.Rotate(Vector3.up, RotateSpeed * Time.deltaTime);
        transform.Rotate(Vector3.right, Mathf.Sin(Time.time * SineSpeed) * SineAmplitude * Time.deltaTime);

    }
}
