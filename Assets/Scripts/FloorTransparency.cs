﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTransparency : MonoBehaviour
{

    public GameObject Worm;

    // Update is called once per frame
    void Update()
    {

        // Check if the worm is sufficiently underneath the terrain to disable the terrain
        // due to blocking player view
        if (Worm.transform.position.y < 0)
        {

            GetComponent<Terrain>().enabled = false;

        }
        else
        {

            GetComponent<Terrain>().enabled = true;

        }


    }

}
