﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormCamera : MonoBehaviour
{

    public GameObject Worm;
    public Vector3 Offset;
    public float Smooth = 0.5f;
    public bool MouseCamera = false;
    public float RotateSpeed = 2f;

    // Update is called once per frame
    void Update()
    {
        
        // if (MouseCamera)
        // {

        //     float rotateHorizontal = Input.GetAxis("Mouse X");
        //     float rotateVertical = -Input.GetAxis("Mouse Y");

        //     transform.RotateAround(Worm.transform.position, Vector3.up, rotateHorizontal * RotateSpeed * Time.deltaTime);
        //     transform.RotateAround(Vector3.zero, transform.right, rotateVertical * RotateSpeed * Time.deltaTime);

        //     transform.position = Worm.transform.position + Offset;

        // }
        // else
        // {

            // Ensure the camera only points towards the worm
            var targetRotation = Quaternion.LookRotation(Worm.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Smooth * Time.deltaTime);

        // }
    }
}
