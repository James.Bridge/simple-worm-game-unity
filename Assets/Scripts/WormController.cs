﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormController : MonoBehaviour
{

    public float MoveSpeed = 5;
    public float SteerSpeed = 180;
    public float VerticalSpeed = 5;
    public int Gap = 10;
    public float SineSpeed = 3;
    public float SineAmplitude = 75;

    public GameObject HeadPrefab;
    public GameObject BodyPrefab;
    public GameObject TailPrefab;
    public GameObject PauseMenu;

    private List<GameObject> BodyParts = new List<GameObject>();
    private List<Vector3> PositionHistory = new List<Vector3>();
    private Quaternion qTo;

    // Start is called before the first frame update
    void Start()
    {
        // GrowHead();
        GrowWorm();
        // GrowTail();
    }

    // Update is called once per frame
    void Update()
    {
        
        // Constantly move forward
        transform.position += transform.forward * MoveSpeed * Time.deltaTime;

        // Horizontal rotations
        float steerHorizontal = Input.GetAxis("Horizontal");
        transform.Rotate(Vector3.up, steerHorizontal * SteerSpeed * Time.deltaTime);
        // Only move in sine manner when no user input
        if (steerHorizontal == 0)
        {

            transform.Rotate(Vector3.up, Mathf.Sin(Time.time * SineSpeed) * SineAmplitude * Time.deltaTime);

        }

        // Vertical rotations
        float steerVertical = Input.GetAxis("Vertical");
        // If user wants to move upwards or downwards we bring the forward vector to the correct direction in the world
        if (steerVertical > 0)
        {

            qTo = Quaternion.FromToRotation(transform.forward, Vector3.up) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, qTo, VerticalSpeed * Time.deltaTime);

        }
        else if (steerVertical < 0)
        {
            qTo = Quaternion.FromToRotation(transform.forward, Vector3.down) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, qTo, VerticalSpeed * Time.deltaTime);
        }
        else
        {

            qTo = Quaternion.FromToRotation(transform.up, Vector3.up) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, qTo, VerticalSpeed * Time.deltaTime);

        }

        // Add to History to update body
        PositionHistory.Insert(0,transform.position);

        int index = 0;
        foreach (var body in BodyParts)
        {

            // Store point and calculate the vector to move body part towards the point
            Vector3 point = PositionHistory[Mathf.Min(index * Gap, PositionHistory.Count - 1)];
            Vector3 vectorDirection = point - body.transform.position;
            body.transform.position += vectorDirection * MoveSpeed * Time.deltaTime;
            body.transform.LookAt(point);

            index++;

        }

        // Pause game if esc is pressed
        if (Input.GetKey("escape"))
        {

            FindObjectOfType<GameManager>().Pause();

        }

    }

    public void GrowWorm()
    {

        GameObject body = Instantiate(BodyPrefab);
        BodyParts.Add(body);

    }

    // private void GrowHead()
    // {

    //     GameObject head = Instantiate(HeadPrefab);
    //     BodyParts.Add(head);

    // }

    // private void GrowTail()
    // {

    //     GameObject tail = Instantiate(TailPrefab);
    //     BodyParts.Add(tail);

    // }

    // void OnCollisionEnter(Collision collision)
    // {

    //     if (collision.gameObject.tag == "Floor")
    //     {

    //         Debug.Log("FLOOR HIT");

    //     }

    // }

}
