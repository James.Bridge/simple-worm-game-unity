# Simple Worm Game Unity

## Description
This repository contains development files and build folders for the simple worm game made using the Unity Game Engine. This game is a 3D Snake clone which allows the snake to traverse in any direction, eat apples to grow and increase score, as well as enemies to avoid.

## Usage
To play the game download the 'Worm Game Build folder' which should contain all required files to run the game locally. Alternatively to play the WebGL version download the 'Worm Game WebGL Build' and host the folder using Node to run the web version.

